import { useState } from "react";
import "./App.css";
import Item from "./components/Item";
import Cart from "./components/Cart";
import RefreshRestart from "./components/RefreshRestart";

let itemData = [
  { id: 1, count: 0 },
  { id: 2, count: 0 },
  { id: 3, count: 0 },
  { id: 4, count: 0 },
];

function App() {
  let [dataState, setDataState] = useState(itemData);
  let incrementQuantity = (itemId) => {
    let data = dataState.map((element, ind) => {
      if (itemId === element.id) {
        element.count++;
        return element;
      } else return element;
    });
    setDataState(data);
  };
  let decrementQuantity = (itemId) => {
    let data = dataState.map((element, ind) => {
      if (element.count > 0 && itemId === element.id) {
        element.count--;
        return element;
      } else return element;
    });
    setDataState(data);
  };
  let deleteItem = (itemId) => {
    let data = dataState.filter((item) => item.id !== itemId);
    setDataState(data);
  };
  let updateItemCount = () => {
    let data = dataState.filter((item) => item.count > 0);
    return data.length;
  };
  let refresh = () => {
    let data = dataState.map((item) => {
      item.count = 0;
      return item;
    });
    setDataState(data);
  };
  let restart = () => {
    if (dataState.length === 0) {
      setDataState(itemData);
    }
  };
  return (
    <>
      <div className="container">
        <Cart update={updateItemCount} />
        <div className="buttons">
          <RefreshRestart refresh={refresh} restart={restart} />
        </div>
        <div className="items">
          {dataState.map((ele, ind) => {
            return (
              <Item
                key={ind}
                itemID={ele.id}
                quantity={ele.count}
                increment={incrementQuantity}
                decrement={decrementQuantity}
                deleteItem={deleteItem}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default App;

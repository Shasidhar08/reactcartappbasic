import React, { useState } from "react";
import { ReactDOM } from "react";

function Counter(props) {
  let [quantity, setQuantity] = useState(props.count);
  // console.log(prop.count + "from counter function");
  let [isDeleted, setDelete] = useState(false);
  let incrementHandler = () => {
    setQuantity(++quantity);
    if (quantity == 1) {
      console.log(props.counters);
      Cart({
        counters: props.counters.map((counter, ind) => {
          if (counter.id == props.className) {
            return { ...counter, count: quantity };
          }
          return counter;
        }),
      });
    }
  };
  let decrementHandler = () => {
    if (quantity > 0) {
      setQuantity(--quantity);
    }
  };
  let deleteHandler = () => {
    setDelete(true);
  };
  return (
    <>
      {!isDeleted && (
        <div className="items">
          <div className="quantity">{quantity ? quantity : "ZERO"}</div>
          <button onClick={incrementHandler}>+</button>
          <button onClick={decrementHandler}>-</button>
          <button onClick={deleteHandler}>Del</button>
        </div>
      )}
    </>
  );
}
function TopContainer() {
  let reloadHandler = (event) => {
    // console.log(event.target.parentElement.nextElementSibling);
    if (document.querySelector(".counter-items").childElementCount == 0) {
      window.location.reload();
    }
  };
  let refreshHandler = (event) => {};
  return (
    <>
      <div className="top">
        <button onClick={refreshHandler}>refresh</button>
        <button onClick={reloadHandler}>reload</button>
      </div>
    </>
  );
}
function Cart(counters) {
  // let [count, setCount] = useState(0);
  if (counters.counters === undefined) {
    // console.log(counters.counters);
    return <>0</>;
  } else {
    console.log(counters.counters);
    let reduceCount = counters.counters.reduce((acc, counter) => {
      console.log(counter.count);
      if (counter.count > 0) {
        return ++acc;
      }
    }, 0);
    // setCount(reduceCount);
    console.log(reduceCount);
    // let cart = React.createElement("div", null, reduceCount);
    // ReactDOM.render([cart], document.querySelector(".cart-item-count"));
  }
}
function Component(props) {
  // console.log(props.counters);
  return (
    <>
      <div className="top">
        <div className="cart-item-count">
          <Cart />
        </div>
        <TopContainer key="top" />
      </div>
      <div className="counter-items">
        {props.counters.map((counter, i) => {
          // console.log(counter.id + " " + counter.count);
          return (
            <Counter
              key={counter.id}
              className={i + 1}
              counters={props.counters}
              count={counter.count}
            />
          );
        })}
      </div>
    </>
  );
}
export default Component;

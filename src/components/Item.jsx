import { useState } from "react";

function Item({ itemID, quantity, increment, decrement, deleteItem }) {
  return (
    <>
      <div className="quantity" itemID={itemID}>
        {quantity}
      </div>
      <button className="plus" onClick={() => increment(itemID)}>
        +
      </button>
      <button className="minus" onClick={() => decrement(itemID)}>
        -
      </button>
      <button className="delete" onClick={() => deleteItem(itemID)}>
        Delete
      </button>
    </>
  );
}

export default Item;

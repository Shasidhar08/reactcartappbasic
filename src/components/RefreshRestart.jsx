function RefreshRestart({ refresh, restart }) {
  return (
    <>
      <button onClick={refresh}>Refresh</button>
      <button onClick={restart}>Restart</button>
    </>
  );
}
export default RefreshRestart;
